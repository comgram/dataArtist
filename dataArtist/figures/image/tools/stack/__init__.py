'''
Operations on image stacks (multiple images)
'''
from ChasePixel import ChasePixel
from Drill import Drill
from PlotTrend import PlotTrend
from Slice import Slice
from Squeeze import Squeeze

show={'simple':False, 'electroluminescence':False}
