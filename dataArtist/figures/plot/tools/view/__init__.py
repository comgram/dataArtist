from Colors import Colors
from ErrorBar import ErrorBar
from Legend import Legend
from LineStyle import LineStyle
from LineWidth import LineWidth
from SymbolColor import SymbolColor
from Symbols import Symbols
from Axes import Axes
from LinkView import LinkView


color = 'blue'
show = True