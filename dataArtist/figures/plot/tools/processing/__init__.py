from Average import Average
from MovingAverage import MovingAverage
from Normalize import Normalize
from RemoveOutliers import RemoveOutliers
from StitchPlots import StitchPlots
from AverageRange import AverageRange

color = 'red'
show = True